import json
import boto3
import os

client = boto3.client('route53')

def delete_route_records(id, iname, instance_ip):
    print(id, iname, instance_ip)
    response = client.change_resource_record_sets(
        HostedZoneId=id,
        ChangeBatch={
            'Changes': [
                {
                  'Action': 'DELETE',
                  'ResourceRecordSet': {
                         'Name': iname,
             'ResourceRecords': [
                             {
                                 'Value': instance_ip
                                }
                         ],
                         'Type': 'A',
                         'TTL': 300
                        }
                    }
             ]
         }
    )


def get_route_records(Id,iname):
    while(iname is not None):
        records = client.list_resource_record_sets(HostedZoneId=Id, StartRecordName=iname, MaxItems='600')

        try:
                for record in records['ResourceRecordSets']:
                    if ( record['Type'] == 'A' and record['Name'] == iname ):
                        for item in record['ResourceRecords']:
                            instance_ip = item['Value']
                            delete_route_records(Id,iname,instance_ip)
                iname = records[iname]

        except Exception as e:
                iname = None

def handler(event, context):
    response = client.list_hosted_zones_by_name()
    zone_data = json.dumps(response)
    zone_domain = json.loads(zone_data)

    InstanceID = event['detail']['instance-id']
    iregion = os.getenv('AWS_REGION', 'us-east-1')
    ec2 = boto3.resource('ec2', region_name=iregion)
    instances = ec2.instances.filter(Filters=[{'Name': 'instance-id', 'Values': [ InstanceID ]}])
    
    for instance in instances:
        if instance.tags is None:
            continue
        else:
            for tags in instance.tags:
                if tags["Key"] == 'env':
                    environment = tags["Value"]
                if tags["Key"] == 'Name':
                    instancename = tags["Value"]
                    instancename = instancename + '.'

            for hosted_zone in zone_domain["HostedZones"]:
                zone_name = hosted_zone["Name"]
                zone_id = hosted_zone["Id"]
                Id = zone_id.split('/')[-1]
                iname = instancename + zone_name
                get_route_records(Id,iname)
